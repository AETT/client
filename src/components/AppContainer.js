import React from 'react'
import { DiffComponent } from './diffComponent';
import ResultContainer from './ResultContainer'

export class AppContainer extends React.Component {
  constructor(params) {
    super(params);
    this.state = {
      result: []
    }
  }

  handleResult = result => this.setState({ result });

  render () {
    return (
      <div>
        <DiffComponent onResult={this.handleResult} />
        {this.state.result.length > 0 ? <ResultContainer data={this.state.result}/> : <div></div>}
      </div>
    )
  }
}