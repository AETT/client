import React from 'react';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';

const rowMapper = (item, index) => (
  <TableRow key={index}>
    <TableRowColumn>{index + 1}</TableRowColumn>
    <TableRowColumn>{item.slice(0, 2)}</TableRowColumn>
    <TableRowColumn>{item.slice(2)}</TableRowColumn>
  </TableRow>
);

export default ({data}) => (
  <Table>
    <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
      <TableRow>
        <TableHeaderColumn>#</TableHeaderColumn>
        <TableHeaderColumn>Status</TableHeaderColumn>
        <TableHeaderColumn>Line</TableHeaderColumn>
      </TableRow>
    </TableHeader>
    <TableBody displayRowCheckbox={false}>
      {data.map(rowMapper)}
    </TableBody>
  </Table>
)