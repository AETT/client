import React from 'react';
import { Tabs, Tab } from 'material-ui/Tabs';
import { TextInputTab, FileUploadTab } from './';

export const DiffComponent = ({ onResult }) => (
  <Tabs onChange={() => onResult([])}>
    <Tab label="Text Input">
      <TextInputTab onResult={onResult}/>
    </Tab>
    <Tab label="File Upload">
      <FileUploadTab onResult={onResult}/>
    </Tab>
  </Tabs>
);