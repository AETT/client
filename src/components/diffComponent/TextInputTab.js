import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import { TextArea } from './';
import CircularProgress from 'material-ui/CircularProgress';

import { handleChange, handleSubmit, isDisabled } from './helpers';

export class TextInputTab extends React.Component {
  constructor(params) {
    super(params);
    this.state = {
      source1: "",
      source2: "",
      isLoading: false
    };
    this.handleChange = handleChange.bind(this);
    this.handleSubmit = handleSubmit.bind(this, 'compareText');
    this.isDisabled = isDisabled.bind(this);
  }

  render = () => {
    return (
      <form className="text-input-tab" onSubmit={this.handleSubmit}>
        <div className="source-inputs">
          <TextArea
            disabled={this.state.isLoading}
            value={this.state.source1}
            floatingLabelText="Source Text"
            onChange={({ target }) => this.handleChange('source1', target.value)}
          />
          <TextArea
            disabled={this.state.isLoading}
            value={this.state.source2}
            floatingLabelText="Updated Text"
            onChange={({ target }) => this.handleChange('source2', target.value)}
          />
        </div>
        <div className="form-controls">
          {this.state.isLoading ?
            <CircularProgress /> :
            <RaisedButton type="submit" label="Compare" disabled={this.isDisabled()} primary={true}/>
          }
        </div>
      </form>
    )
  }
}