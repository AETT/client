import React from 'react';
import { Tab } from 'material-ui/Tabs';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';

import { handleChange, handleSubmit, isDisabled } from './helpers';

export class FileUploadTab extends React.Component {
  constructor(params) {
    super(params);
    this.state = {
      source1: null,
      source2: null,
      isLoading: false
    };
    this.handleChange = handleChange.bind(this);
    this.handleSubmit = handleSubmit.bind(this, 'compareFiles');
    this.isDisabled = isDisabled.bind(this);
  }

  render () {
    return (
      <form className="file-input-tab" onSubmit={this.handleSubmit}>
        <div>
          <label>
            Choose 1st file:
            <input type="file" onChange={({ target }) => this.handleChange('source1', target.files[0])}/>
          </label>
        </div>
        <div>
          <label>
            Choose 2nd file:
            <input type="file" onChange={({ target }) => this.handleChange('source2', target.files[0])}/>
          </label>
        </div>
        <div className="form-controls">
          {this.state.isLoading ?
            <CircularProgress /> :
            <RaisedButton type="submit" label="Compare" disabled={this.isDisabled()} primary={true}/>
          }
        </div>
      </form>
    )
  }
}