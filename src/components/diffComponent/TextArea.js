import React from 'react';
import TextField from 'material-ui/TextField';

export const TextArea = (props) => (
  <TextField
    multiLine={true}
    rows={2}
    rowsMax={4}
    {...props}
  />
);