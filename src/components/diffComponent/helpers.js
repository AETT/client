import diffService from '../../services/diffService'

export function handleChange (source, value) {
  this.setState({
    [source]: value
  });
}

export function handleSubmit (compareMethod, event) {
  event.preventDefault();
  const state = this.state;
  this.setState({
    isLoading: true
  });
  diffService[compareMethod](state.source1, state.source2)
    .then(({ result }) => {
      this.props.onResult(result);
      this.setState({
        isLoading: false
      });
    })
    .catch(() => {
      this.setState({
        isLoading: false
      });
    });
}

export function isDisabled () {
  return !this.state.source1 || !this.state.source2;
}