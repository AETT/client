import 'whatwg-fetch';
import { BASE_URL } from './constants'

class DiffService {
  compareFiles(file1, file2) {
    var data = new FormData()
    data.append('file1', file1);
    data.append('file2', file2);

    return fetch(`${BASE_URL}/compare-files`, {
      method: 'POST',
      body: data
    })
    .then(res => res.json())
    .catch(() => {
      // TODO: Handle error
    });
  }

  compareText(source1, source2) {
    return fetch(`${BASE_URL}/compare`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ source1, source2 })
    })
    .then(res => res.json())
    .catch(() => {
      // TODO: Handle error
    });
  }
}

export default new DiffService();